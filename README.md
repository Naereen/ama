# :dizzy: Ask me anything! :sparkles:

:speech_balloon: [Ask a question](../../issues/new) | :book: [Read questions](../../issues?q=is%3Aissue+is%3Aclosed+sort%3Aupdated-desc)

I get questions by email, occasionally. This way anyone can read the answer!

[![AMA](https://img.shields.io/badge/ask%20me-anything-1abc9c.svg)](https://bitbucket.org/lbesson/ama)

Anything means *anything*. Personal questions. Money. Work. Life. Code.
Cooking. Traveling. Sports. Teaching. Pokémon. Whatever. :joy:

### :memo: Guidelines

 - :mag: Ensure your question has not already been answered.
 - :memo: Use a succinct title and description.
 - :bug: Bugs & feature requests should be opened on the relevant issue tracker (i.e., on the issue tracker of the corresponding [Bitbucket](https://bitbucket.org/lbesson/) or [GitHub](https://github.com/Naereen/) repository).
 - :signal_strength: Support questions are better asked on [Stack Overflow](https://stackoverflow.com/) and maths questions on [Math Exchange](https://math.stackexchange.com/).
 - :blush: Be nice, civil and polite ([as always](http://contributor-covenant.org/version/1/4/)).
 - :heart_eyes: If you include at least one emoji in your question, the feedback might come faster!

### *In French? En Français ?*
[In English / En anglais](https://bitbucket.org/lbesson/ama)
or [In French / En Français](https://bitbucket.org/lbesson/ama.fr).

----

### Links

 - [Read more AMAs.](https://github.com/sindresorhus/amas)
 - [What's an AMA?](https://en.wikipedia.org/wiki/Reddit#IAmA_and_AMA)

### :scroll: License
This (small) repository is published under the terms of the [MIT license](http://lbesson.mit-license.org/) (file [LICENSE.txt](LICENSE.txt)).

[![Analytics](https://ga-beacon.appspot.com/UA-38514290-17/bitbucket.org/lbesson/ama/README.md?pixel)](https://bitbucket.org/lbesson/ama/)
